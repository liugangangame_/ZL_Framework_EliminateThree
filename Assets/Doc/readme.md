# ZL_Framework使用文档

## 快速开始

打开 Scences/MainScene 场景
找到场景中的GameEnter物体，打开此物体挂载的GameEnter脚本
这个脚本就是全局开始的入口，所有的逻辑都从这个脚本开始

> 框架里包含了一个小游戏，可以快速帮助理解

> 如果打开项目报错，请到 PlayerSettings - OtherSettings ，设置 API Level 为 .Net4.x

### 组件说明

#### Lua - LuaManager

* LuaManager：管理XLua的组件，确保全局唯一的LuaEnv
* LuaBehaviour: 在lua中模拟unity的生命周期

#### UI - UIManager

打开一个UI，只需要一行代码：
``` csharp
UIManager.Instance.OpenUI(UIConst.UI_LOGO_PAGE);
```
使用方法：
1. 把需要管理的UI先在Assets/AssetBundles/UIConfig.json中填好对应的名称和资源目录
2. 调用 UIManager.Instance.OpenUI("UIName"); 即可

> 打开需要执行Lua脚本的UI需要用LuaOpenUI

关闭UI:
``` csharp
UIManager.Instance.CloseUI(UIName);
```

#### AssetBundle资源管理 - AssetBundlesManager

统一管理资源加载的类：AssetBundlesManager
包括AssetBundle包和Resources资源，在项目中所有用到的资源都通过此管理器加载。
已经内置写好了GameObject，Texture，AudioClip, Sprite的加载方法，直接调用即可

> 资源名称需要填写Asset路径下的完整路径，带后缀

#### 音频 - AudioManager

统一的音频播放管理
利用对象池，管理所有音频的播放

#### 网络 - HttpRequestManager

http协议的网络请求管理，引入了第三方插件BestHttp，也保留了UnityWebRequest方式

#### 消息分发机制 - MsgDispatcherController

利用消息机制，降低逻辑耦合度
使用方法请点[这里](https://gitee.com/zlr710/SimpleMsgDispatcher)

#### 计时 - TimerTrigger

使用方便的链式计时器（使用方式类似DOTween）

#### Log - LogTool

可以直接打印在屏幕上的Log，包含了帧数和Lua内存的显示

## API

请参考脚本注释

## 打包

开发完成需要打资源包测试时流程：

1. 点击工具栏 **AssetBundle-设置AB名称-AssetBundles路径下所有资源** 按钮，给所有资源设置AssetBundle名称
2. 点击工具栏 **AssetBundle-打包安卓资源** 按钮，打包AssetBundle到StreamingAssets目录
3. 点击工具栏 **AssetBundle-打包Lua脚本** 按钮，打包Lua脚本到StreamingAssets目录
4. 点击工具栏 **XLua-Generate Code** 按钮，生成XLua静态脚本
5. 打安装包

## 环境说明

* 开发调试的话，让 IsSimulate = true，有改动的话直接运行就可以看到
* 开发完成测试资源打包的话，可以把 IsSimulate = false，然后 IsEmulation = false，此时将从 StreamingAssets 目录下加载资源及Lua脚本
* 如果想测试真实环境，把 IsSimulate = false，然后 IsEmulation = true，此时将会热更新资源包，并从缓存目录加载资源及Lua脚本
* 如果想打包移动端测试，可以 IsEmulation 设置为 false，这样在移动端也从StreamingAssets目录下加载资源及Lua脚本
* 宏：DEV，管理屏幕Log输出，也可以用作其他仅开发时的宏判断