--------------------------------------------
-- Copyright © 2018 luaide-lite
-- File: GameStartPage.lua
-- Author: zhaoliang
-- Date: 2020-05-12 05:08:39
-- Desc:
--------------------------------------------
GameStartPage = {}

local transform
local gameObject
local startBtn

GameStartPage.onOpen = function(param)
    transform = GameStartPage.transform
    gameObject = GameStartPage.gameObject

    MsgDispatcherController:SendMsg(MsgNameList.MSG_CLOSE_LOGO_PAGE)
    -- 给开始按钮添加游戏开始事件
    startBtn = transform:Find("start").gameObject:GetComponent(typeof(UI.Button))
    startBtn.onClick:AddListener(GameStartPage.StartGame)
end

-- 游戏开始
GameStartPage.StartGame = function()
    print("GameStartPage")

    AudioManager:PlayCommonAudio("/Click.wav")

    AssetBundlesManager:LoadInstantiateGameObject(
        AppConfig.ResPath .. "/Game",
        AppConfig.ResPath .. "/Game/Game.prefab",
        function(str, obj)
            UIManager:LuaOpenUI(UIName.UI_GAME_UI, nil,
                function(ui)
                    MsgDispatcherController:SendMsg(MsgNameList.MSG_GAME_START)
                end
            )
        end
    )
    UIManager:CloseUI(GameStartPage.UIName)
end

GameStartPage.dispose = function()
    startBtn.onClick:RemoveAllListeners()
end

return GameStartPage
