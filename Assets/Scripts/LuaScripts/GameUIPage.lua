--------------------------------------------
-- Copyright © 2018 luaide-lite
-- File: GameScore.lua
-- Author: zhaoliang
-- Date: 2020-05-13 10:26:52
-- Desc:
--------------------------------------------
GameUIPage = {}

local transform
local gameObject
local totalScoreText
local curAddScoreText
local score
local hint_btn

GameUIPage.onOpen = function(param)
    transform = GameUIPage.transform
    gameObject = GameUIPage.gameObject

    MsgDispatcherController:RegisterMsg(MsgNameList.MSG_UPDATE_SCORE, GameUIPage.UpdateScore)

    score = 0
    totalScoreText = transform:Find("scoreObj/score_").gameObject:GetComponent(typeof(UI.Text))
    curAddScoreText = transform:Find("add_score").gameObject:GetComponent(typeof(UI.Text))
    hint_btn = transform:Find("hint_btn").gameObject:GetComponent(typeof(UI.Button))
    hint_btn.onClick:AddListener(GameUIPage.GameHint)
end

-- 更新分数
GameUIPage.UpdateScore = function(s)
    local curScore = s.param
    score = score + curScore
    totalScoreText.text = tostring(score)

    curAddScoreText.text = " + " .. tostring(curScore)
    curAddScoreText:DOFade(1, 0)
    curAddScoreText:DOFade(0, 2)
end

GameUIPage.GameHint = function()
    MsgDispatcherController:SendMsg(MsgNameList.MSG_GAME_HINT)
end

GameUIPage.dispose = function()
    hint_btn.onClick:RemoveAllListeners()
end

return GameUIPage