--------------------------------------------
-- Copyright © 2018 luaide-lite
-- File: Main.lua
-- Author: zhaoliang
-- Date: 2020-05-09 04:00:06
-- Desc:
--------------------------------------------
require("define")
require("RequireList")

local yield_return = require("coroutine_run").yield_return
local interval = UnityEngine.WaitForSeconds(0.01)
local luaBehaviour

function awake()
    LogTool:Log("Lua Start")

    startRequire()
end

-- 预加载lua脚本
function startRequire()
    -- 协程用法
    local co =
        coroutine.create(
        function()
            for k, v in pairs(RequireList.RequireList) do
                require(v)
                yield_return(interval)
            end
            startGame()
        end
    )
    coroutine.resume(co)
end

function startGame()
    AudioManager:PlayBGM(AudioManager.CommonAudioPath, AudioManager.CommonAudioPath .. "/bgm.ogg", true, 0.5)
    UIManager:LuaOpenUI(UIName.UI_GAME_START)
end
