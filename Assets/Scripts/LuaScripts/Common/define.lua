UnityEngine = CS.UnityEngine
UI = UnityEngine.UI
Mathf = UnityEngine.Mathf
Time = UnityEngine.Time
GameObject = CS.UnityEngine.GameObject
Vector3 = UnityEngine.Vector3
Vector2 = UnityEngine.Vector2
Rect = UnityEngine.Rect
Color = UnityEngine.Color
Dictionary_String_Object = CS.System.Type.GetType('System.Collections.Generic.Dictionary`2[[System.String, mscorlib],[System.Object, mscorlib]],mscorlib')
Dictionary_String_String = CS.System.Type.GetType('System.Collections.Generic.Dictionary`2[[System.String, mscorlib],[System.String, mscorlib]],mscorlib')

KeyCode = CS.UnityEngine.KeyCode
EventSystem = CS.UnityEngine.EventSystems.EventSystem
WWW = CS.UnityEngine.WWW
String  = CS.System.string
LuaBehaviour = CS.ZL_Framework.LuaBehaviour
MsgDispatcherController = CS.ZL_Framework.MsgDispatcherController.Instance
AssetBundlesManager = CS.ZL_Framework.AssetBundlesManager.Instance
UIManager = CS.ZL_Framework.UIManager.Instance
AudioManager = CS.ZL_Framework.AudioManager.Instance
AppConfig = CS.ZL_Framework.AppConfig
UIType = CS.ZL_Framework.UIType
LogTool = CS.ZL_Framework.LogTool.Instance
Timer = CS.ZL_Framework.Timer

logW = function(format, ...)
	local callstack = debug.traceback()
	format = string.format("%s\n%s", format, callstack)
	UnityEngine.Debug.LogWarning("LUA Warning: " .. format, ...)
end

logE = function(format, ...)
	local callstack = debug.traceback()
	format = string.format("%s\n%s", format, callstack)
	UnityEngine.Debug.LogError("LUA Error: " .. format, ...)
end