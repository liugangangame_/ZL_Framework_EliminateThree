--------------------------------------------
-- Copyright © 2018 luaide-lite
-- File: MsgNameList.lua
-- Author: zhaoliang
-- Date: 2020-05-13 09:30:02
-- Desc:
--------------------------------------------
MsgNameList = {}

MsgNameList.MSG_GAME_START = 1000
MsgNameList.MSG_CLOSE_LOGO_PAGE = 1001
MsgNameList.MSG_GAME_HINT = 1005
MsgNameList.MSG_UPDATE_SCORE = 1010