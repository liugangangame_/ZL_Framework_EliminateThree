local util = require 'util'

local coroutine = CS.LuaCoroutiner.Instance

local function async_yield_return(to_yield, cb)
    coroutine:YieldAndCallback(to_yield, cb)
end

return {
    yield_return = util.async_to_sync(async_yield_return)
}