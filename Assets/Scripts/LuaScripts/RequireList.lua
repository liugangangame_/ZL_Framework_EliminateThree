--------------------------------------------
-- Copyright © 2018 luaide-lite
-- File: RequireList.lua
-- Author: zhaoliang
-- Date: 2020-05-09 04:04:56
-- Desc:
--------------------------------------------
RequireList = {}

-- 填所有需要预加载的lua脚本名称
RequireList.RequireList = {
    "MsgNameList",
    "UIName"
}

local this = RequireList
