﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace ZL_Framework.Game
{
    /// <summary>
    /// 染色图案
    /// </summary>
    public class ShapeItemDye : ShapeItem
    {
        public int dyeIndex;

        public override void DestroyShape(TweenCallback tweenCallback)
        {
            base.DestroyShape(tweenCallback);
            transform.DOShakeScale(1.5f, 0.3f).OnComplete(() =>
            {
                tweenCallback.Invoke();
                Destroy(gameObject);
            });
        }

        /// <summary>
        /// 染色
        /// </summary>
        /// <param name="_dyeNum"></param>
        public void Dye(int _dyeNum)
        {
            List<ShapeItem> newBoard = new List<ShapeItem>();
            while (newBoard.Count < _dyeNum)
            {
                int rx = Random.Range(0, mBoardManager.xSize);
                int ry = Random.Range(0, mBoardManager.ySize);

                ShapeItem item = mBoardManager.GetShapeItem(new Vector2(rx, ry));
                if (!newBoard.Contains(item) && item.shapeItenType == ShapeItenType.Normal)
                {
                    newBoard.Add(item);
                }
            }

            mMouseInputer.CloseInput();
            StartCoroutine(DyeItem(newBoard));
        }

        public GameObject point;
        private IEnumerator DyeItem(List<ShapeItem> _dyeItem)
        {
            int max = _dyeItem.Count;
            for (int i = 0; i < max; i++)
            {
                ShapeItem item = _dyeItem[i];
                GameObject point_item = Instantiate(point, transform);
                point_item.transform.SetParent(mBoardManager.transform);
                point_item.transform.DOMove(item.transform.position, 0.5f)/*.SetSpeedBased()*/.SetEase(Ease.Linear).OnComplete(() =>
                {
                    Destroy(point_item);
                    item.ChangeSprite(dyeIndex);
                });
                yield return new WaitForSeconds(0.2f);
            }

            mBoardManager.CheckCanRemoveShapeMethod();
        }
    }
}