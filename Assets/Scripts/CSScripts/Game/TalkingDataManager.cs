﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZL_Framework;

[StartManagerAttribute]
public class TalkingDataManager : MonoSingleton<TalkingDataManager>
{
    private string ChannelId =
#if DEV
        "Develop";
#else
        "Test";
#endif
    private string AppId = "E3DA89A32E664129BC9882D7292B6A0D";

    private TDGAAccount account;

    protected override void Init()
    {
        //TalkingDataGA.SetVerboseLogDisabled();
        TalkingDataGA.BackgroundSessionEnabled();
        TalkingDataGA.OnStart(AppId, ChannelId);
        account = TDGAAccount.SetAccount(TalkingDataGA.GetDeviceId());
        if (account != null)
        {
            account.SetAccountType(AccountType.ANONYMOUS);
        }
        LogTool.Instance.Log("TalkingData初始化完成，AppId:" + AppId + " ChannelId:" + ChannelId);
        LogTool.Instance.Log("DeviceId:" + TalkingDataGA.GetDeviceId());
    }

    /// <summary>
    /// 开始某关
    /// </summary>
    /// <param name="levelId"></param>
    public void BeginLevel(int levelId)
    {
        LogTool.Instance.Log("TalkingData Begon Level " + levelId);
        TDGAMission.OnBegin("Level " + levelId);
    }

    /// <summary>
    /// 完成某关
    /// </summary>
    /// <param name="levelId"></param>
    public void CompleteLevel(int levelId)
    {
        LogTool.Instance.Log("TalkingData Complete Level " + levelId);
        TDGAMission.OnCompleted("Level " + levelId);
    }

    /// <summary>
    /// 过关失败
    /// </summary>
    /// <param name="levelId"></param>
    /// <param name="levelFailedCause">失败原因</param>
    public void FailedLevel(int levelId, LevelFailedCause levelFailedCause)
    {
        string cause = "";
        switch (levelFailedCause)
        {
            case LevelFailedCause.Unsolvable:
                cause = "无解";
                break;
            case LevelFailedCause.Quit:
                cause = "退出";
                break;
            case LevelFailedCause.Skip:
                cause = "跳过";
                break;
            case LevelFailedCause.SelectLeve:
                cause = "选关";
                break;
            case LevelFailedCause.Replay:
                cause = "重新开始";
                break;
        }
        LogTool.Instance.Log("TalkingData Failed Level " + levelId + " Cause:" + cause);
        TDGAMission.OnFailed("Level " + levelId, cause);
    }

    /// <summary>
    /// 自定义事件（重新开始）
    /// </summary>
    /// <param name="levelId"></param>
    public void OnReplay(int levelId)
    {
        LogTool.Instance.Log("TalkingData Re Play Level " + levelId);
        //Dictionary<string, object> dic = new Dictionary<string, object>
        //    {
        //        { "levelId",levelId }
        //    };
        //TalkingDataGA.OnEvent("Re_Play", dic);
    }

    /// <summary>
    /// 自定义事件（加管）
    /// </summary>
    /// <param name="levelId"></param>
    public void OnAddGameItem(int levelId)
    {
        LogTool.Instance.Log("TalkingData Add GameItem Level " + levelId);
        //Dictionary<string, object> dic = new Dictionary<string, object>
        //    {
        //        { "levelId",levelId }
        //    };
        //TalkingDataGA.OnEvent("Add_GameItem", dic);
    }

    protected override void Destroy()
    {
        TalkingDataGA.OnEnd();
    }

    public enum LevelFailedCause
    {
        Unsolvable,
        Quit,
        Skip,
        SelectLeve,
        Replay
    }
}