﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace ZL_Framework.Game
{
    /// <summary>
    /// 图案
    /// </summary>
    public class ShapeItem : MonoBehaviour
    {
        public ShapeItenType shapeItenType = ShapeItenType.Normal;  //图形的类型
        public int shapeIndex;  //图形的图案序号
        public SpriteRenderer shapeSprite;
        public SpriteRenderer shapeBg;
        public BoxCollider shapeCollider;
        public bool isActive;   //是否是当前可以被选中的图形
        public Vector2 shapePos;    //当前图形所在的坐标
        public List<Sprite> shapeSprites;

        private LineRenderer mLineRenderer;
        private Color bgColor;
        private int bgLayerBak, shapeLayerBak;

        protected MouseInputer mMouseInputer;
        protected BoardManager mBoardManager;

        private void Start()
        {
            mMouseInputer = MouseInputer.Instance;
            mLineRenderer = GetComponent<LineRenderer>();
            bgColor = shapeBg.color;
            bgLayerBak = shapeBg.sortingOrder;
            shapeLayerBak = shapeSprite.sortingOrder;
        }

        public virtual void InitShape(BoardManager _boardManager, Transform _parent, bool _isLeapMove, float _fallSpeed, AnimationCurve _fallCurve, Vector2 _size)
        {
            SetBoardManager(_boardManager);
            transform.parent = _parent;
            //SetShapeColliderSize(_size);

            if (_isLeapMove)
            {
                transform.DOMoveY(transform.position.y + 5, _fallSpeed).From().SetEase(_fallCurve);
            }
        }

        public void SetShapeItemPos(Vector2 _pos)
        {
            shapePos = _pos;
        }

        public void SetShapeItemPos(int _x, int _y)
        {
            shapePos = new Vector2(_x, _y);
        }

        public void SetBoardManager(BoardManager _boardManager)
        {
            mBoardManager = _boardManager;
        }

        public void SetItemSortingLayer(int _layer)
        {
            shapeSprite.sortingOrder = _layer;
            shapeBg.sortingOrder = _layer - 1;
        }

        public void SetShapeColliderSize(Vector2 _size)
        {
            shapeCollider.size = _size;
        }

        public void SetColor(Color _color)
        {
            //return;
            if (_color == Color.white)
            {
                _color = bgColor;
            }
            shapeBg.color = _color;
        }

        public void HintShape()
        {
            shapeBg.DOFade(1, 0.3f).SetEase(Ease.Flash).SetLoops(4, LoopType.Yoyo);
        }

        public virtual void DestroyShape(TweenCallback tweenCallback)
        {
        }

        /// <summary>
        /// 线特效显示
        /// </summary>
        /// <param name="prePos"></param>
        public void SetLineShow(Vector3 prePos, float lineWidth, Color lineColor)
        {
            mLineRenderer.positionCount = 2;
            mLineRenderer.startColor = lineColor;
            mLineRenderer.endColor = lineColor;
            mLineRenderer.startWidth = lineWidth;
            mLineRenderer.endWidth = lineWidth;
            mLineRenderer.SetPosition(0, prePos);
            mLineRenderer.SetPosition(1, transform.position);
        }

        public virtual void ChangeSprite(int _index)
        {
        }

        public virtual void ResetShapeItem()
        {
            if (mLineRenderer != null)
            {
                mLineRenderer.positionCount = 0;
            }
            if (shapeBg != null && shapeSprite != null)
            {
                shapeBg.sortingOrder = bgLayerBak;
                shapeSprite.sortingOrder = shapeLayerBak;
            }
        }
    }

    public enum ShapeItenType
    {
        Normal,
        Dye
    }
}