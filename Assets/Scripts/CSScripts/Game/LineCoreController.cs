﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using XLua;
using ZLMsg;

namespace ZL_Framework.Game
{
    /// <summary>
    /// 操作控制
    /// </summary>
    public class LineCoreController : MonoBehaviour, IMsgReceiver, IMsgSender
    {
        public LayerMask layerMask; //可以识别的层级
        public int removeMinNum = 2; //最小可消除个数
        public float lineWidth = 0.5f; //线特效宽度
        public Color lineColor = Color.blue; //线特效颜色

        [SerializeField]
        private BoardManager m_BoardManager;

        private ShapeItem perShapeItem, curShapeItem; //本次选中的图形,当前划到的图形
        private Transform curShape; //本次选中的图形 TF

        private List<ShapeItem> curCanLineShapeItem; //当前可以划的图形列表
        private MouseInputer mMouseInputer;
        private List<ShapeItem> shapes; //当前已经划到的图形列表
        private int score;

        private void Awake()
        {
            this.RegisterLogicMsg(MsgName.MSG_GAME_START, StartGame);
        }

        private void StartGame(IMsgParam obj)
        {
            LineCoreStart();
        }

        public void LineCoreStart()
        {
            mMouseInputer = MouseInputer.Instance;
            mMouseInputer.Init(true, 3, layerMask);
            mMouseInputer.onClickObjDown = OnClickDown;
            mMouseInputer.onClickObjUp = OnClickUp;
            mMouseInputer.onEnter = OnEnter;
            shapes = new List<ShapeItem>();
        }

        private void OnEnter(GameObject arg0)
        {
            if (Input.GetMouseButton(0))
            {
                if (m_BoardManager != null && arg0 != null)
                {
                    JudgeShape(arg0);
                }
                else
                {
                    Debug.Log("结束");
                    curShapeItem = null;
                    LineEnd();
                }
            }
        }

        /// <summary>
        /// 比较图形
        /// </summary>
        /// <param name="arg0"></param>
        private void JudgeShape(GameObject arg0)
        {
            if (arg0.transform == curShape) //如果与选中的图形相同
            {
                curShapeItem = null;
                return;
            }
            if (curShapeItem == null)
            {
                curShapeItem = arg0.GetComponent<ShapeItem>();
            }
            if (!curShapeItem.isActive || shapes.Contains(curShapeItem) || curShapeItem.shapeItenType != ShapeItenType.Normal) //如果此图形可以被选择
            {
                curShapeItem = null;
                return;
            }
            if (!curCanLineShapeItem.Contains(curShapeItem))
            {
                Debug.Log("不在可被选中范围");
                curShapeItem = null;
                return;
            }
            if (curShapeItem.shapeIndex == perShapeItem.shapeIndex) //如果此图形与选中图形相同
            {
                AddShape();
                curCanLineShapeItem = m_BoardManager.GetCanLineShapeItem(curShapeItem, true);
                curShapeItem = null;
            }
            else
            {
                curShapeItem = null;
            }
        }

        private void OnClickDown(InputData arg0)
        {
            shapes?.Clear();
            curCanLineShapeItem?.Clear();
            if (m_BoardManager == null || arg0.target == null)
            {
                return;
            }

            curShape = arg0.target;
            perShapeItem = curShape.GetComponent<ShapeItem>();
            shapes.Add(perShapeItem);
            curCanLineShapeItem = m_BoardManager.GetCanLineShapeItem(perShapeItem, true);
        }

        private void OnClickUp(InputData arg0)
        {
            shapes?.Clear();
            curCanLineShapeItem?.Clear();
            if (m_BoardManager == null || arg0.target == null)
            {
                return;
            }

            curShape = arg0.target;
            curShapeItem = curShape.GetComponent<ShapeItem>();
            if (curShapeItem.shapeItenType != ShapeItenType.Normal)
            {
                if (curShapeItem == perShapeItem)
                {
                    ShapeSkill(curShapeItem);
                    shapes.Add(perShapeItem);
                }
                StartCoroutine(m_BoardManager.LineEnd(shapes));
            }
        }

        private void ShapeSkill(ShapeItem _shape)
        {
            if (_shape.shapeItenType == ShapeItenType.Dye)
            {
                ShapeItemDye dyeItem = _shape as ShapeItemDye;
                dyeItem.Dye(5);
            }
        }

        /// <summary>
        /// 添加到图形列表中
        /// </summary>
        private void AddShape()
        {
            curShapeItem.SetItemSortingLayer(7);
            shapes.Add(curShapeItem);
            curShapeItem.SetLineShow(shapes[shapes.Count - 2].transform.position, lineWidth, lineColor);
        }

        /// <summary>
        /// 结束本次操作
        /// </summary>
        private void LineEnd()
        {
            if (!isResponse)
            {
                return;
            }
            m_BoardManager.SetShapeActiveFalse();
            for (int i = 0; i < shapes.Count; i++)
            {
                shapes[i].ResetShapeItem();
            }
            if (shapes.Count >= removeMinNum)
            {
                StartCoroutine(m_BoardManager.LineEnd(shapes));
                int curAdd = 0;
                for (int i = 0; i < shapes.Count; i++)
                {
                    curAdd += i + 1;
                }

                MsgParam<int> param = new MsgParam<int>();
                param.SetParam(curAdd);
                this.SendLogicMsg(MsgName.MSG_UPDATE_SCORE, param);
            }
            shapes?.Clear();
            curCanLineShapeItem?.Clear();
        }

        private bool isResponse;
        private void Update()
        {
            if (!BoardManager.IS_STARTGAME)
            {
                return;
            }
            if (Input.GetMouseButtonUp(0))
            {
                LineEnd();
            }
            if (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))
            {
#if UNITY_EDITOR
                isResponse = !EventSystem.current.IsPointerOverGameObject();
#else
            isResponse = !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId);
#endif
            }
            if (mMouseInputer == null)
            {
                return;
            }
            mMouseInputer.CheckInteracting();
        }
    }
}