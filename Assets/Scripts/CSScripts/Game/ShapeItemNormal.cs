﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace ZL_Framework.Game
{
    /// <summary>
    /// 普通图案
    /// </summary>
    public class ShapeItemNormal : ShapeItem
    {
        public override void InitShape(BoardManager _boardManager, Transform _parent, bool _isLeapMove, float _fallSpeed, AnimationCurve _fallCurve, Vector2 _size)
        {
            base.InitShape(_boardManager, _parent, _isLeapMove, _fallSpeed, _fallCurve, _size);

            int index = Random.Range(0, shapeSprites.Count);
            Sprite s = shapeSprites[index];
            shapeSprite.sprite = s;
            shapeIndex = index;
        }

        public override void DestroyShape(TweenCallback tweenCallback)
        {
            transform.DOScale(0, 0.2f).OnComplete(() =>
            {
                tweenCallback.Invoke();
                Destroy(gameObject);
            });
        }

        public override void ChangeSprite(int _index)
        {
            base.ChangeSprite(_index);

            Sprite s = shapeSprites[_index];
            shapeSprite.sprite = s;
            shapeIndex = _index;
        }
    }
}