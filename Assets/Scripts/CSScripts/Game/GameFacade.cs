﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using ZL_Framework;
using ZLMsg;

public class GameFacade : MonoBehaviour, IMsgSender, IMsgReceiver
{
    protected int gameType;

    public virtual void StartGame()
    {
    }

    public virtual void Success()
    {
        LogTool.Instance.Log("过关");
    }

    public virtual void Failed()
    {
        LogTool.Instance.Log("失败");
    }

    public virtual void BackOff()
    {
    }

    public virtual void Replay()
    {
    }

    public virtual void GameFinish()
    {

    }

    public IEnumerator CaptureScreenshot(string captFolder, string fileName)
    {
        yield return new WaitForEndOfFrame();

#if !DEV
        Texture2D screenShot = ScreenCapture.CaptureScreenshotAsTexture();
        screenShot.Apply();
        byte[] bytes = screenShot.EncodeToPNG();
        if(!Directory.Exists(captFolder))
        {
            Directory.CreateDirectory(captFolder);
        }
        string filename = captFolder + fileName;
        File.WriteAllBytes(filename, bytes);
#endif
    }
}