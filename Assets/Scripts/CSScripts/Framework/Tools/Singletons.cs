﻿using System;

/// <summary>
/// 
/// 范型单例基类
/// 
/// add by Minghua.ji
/// 
/// </summary>
public abstract class Singletons<T> where T : Singletons<T>, new()
{

    #region 静态成员
    private static T instance;

    private static void CheckInstanceNull()
    {
        if (instance == null)
        {
            instance = new T();
        }
    }

    public static T Instance
    {
        get
        {
            CheckInstanceNull();
            return instance;
        }
    }
    #endregion

    internal Singletons()
    {
        if (instance != null)
        {
        }
    }

    public virtual void Dispose()
    {
    }
}