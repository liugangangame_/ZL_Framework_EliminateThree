﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.Security.Cryptography;
using System.Text;
using System;
using System.IO;
using System.Text.RegularExpressions;

namespace ZL_Framework
{
    /// <summary>
    /// 功能拓展
    /// </summary>
    public static class Extensions
    {
        #region PIVO锚点位置
        public static Vector2 Pivot_Center { get { return new Vector2(0.5f, 0.5f); } }
        public static Vector2 Pivot_Top { get { return new Vector2(0.5f, 1); } }
        public static Vector2 Pivot_Bottom { get { return new Vector2(0.5f, 0); } }
        public static Vector2 Pivot_Left { get { return new Vector2(0, 0.5f); } }
        public static Vector2 Pivot_Right { get { return new Vector2(1, 0.5f); } }
        public static Vector2 Pivot_TopLeft { get { return new Vector2(0, 1); } }
        public static Vector2 Pivot_TopRight { get { return Vector2.one; } }
        public static Vector2 Pivot_BottomLeft { get { return Vector2.zero; } }
        public static Vector2 Pivot_BottomRight { get { return new Vector2(1, 0); } }
        #endregion

        /// <summary>
        /// 获取文件长度
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static long GetFileLength(string file)
        {
            FileStream fs = new FileStream(file, FileMode.Open);
            long length = fs.Length;
            fs.Close();
            fs.Dispose();
            return length;
        }

        /// <summary> 
        /// 获取时间戳 
        /// </summary> 
        /// <returns></returns> 
        public static long GetTimeStamp()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds);
        }

        #region string
        public static byte[] GetBytes(this string str)
        {
            return Encoding.UTF8.GetBytes(str);
        }

        public static string GetString(this byte[] byts)
        {
            return Encoding.UTF8.GetString(byts);
        }

        /// <summary>  
        /// Unicode字符串转为正常字符串0  
        /// </summary>  
        /// <param name="srcText">Unicode字符(格式为："\uxxxx" ，举例："\u67f3_abc123")</param>  
        /// <returns>正常字符串</returns>  
        public static string UnicodeToString(this string srcText)
        {
            return Regex.Unescape(srcText);
        }

        /// <summary>  
        /// 字符串转为UniCode码字符串  
        /// </summary>  
        /// <param name="s">正常字符串</param>  
        /// <returns>Unicode字符(格式为："\uxxxx" ，举例："\u67f3_abc123")</returns>  
        public static string StringToUnicode(this string s)
        {
            char[] charbuffers = s.ToCharArray();
            byte[] buffer;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < charbuffers.Length; i++)
            {
                buffer = Encoding.Unicode.GetBytes(charbuffers[i].ToString());
                sb.Append(string.Format("//u{0:X2}{1:X2}", buffer[1], buffer[0]));
            }
            return sb.ToString();
        }

        /// <summary>
        /// 字符串转为UTF-8
        /// </summary>
        /// <param name="s">正常字符串</param>
        /// <returns>UTF-8字符串</returns>
        public static string StringToUTF8(this string s)
        {
            byte[] buffer = Encoding.GetEncoding("utf-8").GetBytes(s);
            string str = "";

            foreach (byte b in buffer) str += string.Format("%{0:X}", b);
            return str;
        }

        /// <summary>
        /// UTF8转换成GB2312
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string utf8_gb2312(this string text)
        {
            //声明字符集   
            Encoding utf8, gb2312;
            //utf8   
            utf8 = Encoding.GetEncoding("utf-8");
            //gb2312   
            gb2312 = Encoding.GetEncoding("gb2312");
            byte[] utf;
            utf = utf8.GetBytes(text);
            utf = Encoding.Convert(utf8, gb2312, utf);
            //返回转换后的字符   
            return gb2312.GetString(utf);
        }
        #endregion

        #region MD5
        /// <summary>
        /// 计算字符串的MD5值
        /// </summary>
        public static string GetMD5(this string data)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] dataBytes = Encoding.UTF8.GetBytes(data.ToString());
            byte[] md5Bytes = md5.ComputeHash(dataBytes);
            string dataMD5 = BitConverter.ToString(md5Bytes).Replace("-", "").ToLower();
            return dataMD5;
        }

        /// <summary>
        /// 计算文件的MD5值
        /// </summary>
        public static string GetFileMD5(string file)
        {
            try
            {
                FileStream fs = new FileStream(file, FileMode.Open);
                MD5 md5 = new MD5CryptoServiceProvider();
                byte[] retVal = md5.ComputeHash(fs);
                fs.Close();

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < retVal.Length; i++)
                {
                    sb.Append(retVal[i].ToString("x2"));
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("Extensions.GetFileMD5() fail, error:" + ex.Message);
            }
        }
        #endregion

        #region LitjsonExtensions
        public static JsonData Decode(this string _data)
        {
            return JsonMapper.ToObject(_data);
        }

        public static string Encode(this JsonData _json)
        {
            return _json.ToJson();
        }

        public static string Encode(this object _jsonObj)
        {
            return JsonMapper.ToJson(_jsonObj);
        }

        public static T EncodeToObject<T>(this string _json)
        {
            return JsonMapper.ToObject<T>(_json);
        }

        public static JsonData AddField(this JsonData _jsonData, string _key, JsonData _value)
        {
            _jsonData[_key] = _value;
            return _jsonData;
        }

        public static JsonData AddField(this JsonData _jsonData, string _key, string _value)
        {
            _jsonData[_key] = _value;
            return _jsonData;
        }

        public static JsonData AddField(this JsonData _jsonData, string _key, int _value)
        {
            _jsonData[_key] = _value;
            return _jsonData;
        }

        public static JsonData AddField(this JsonData _jsonData, string _key, float _value)
        {
            _jsonData[_key] = _value;
            return _jsonData;
        }

        public static JsonData AddField(this JsonData _jsonData, string _key, bool _value)
        {
            _jsonData[_key] = _value;
            return _jsonData;
        }

        public static JsonData AddField(this JsonData _jsonData, string _key, double _value)
        {
            _jsonData[_key] = _value;
            return _jsonData;
        }
        #endregion
    }
}