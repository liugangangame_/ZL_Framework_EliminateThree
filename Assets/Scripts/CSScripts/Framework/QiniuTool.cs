﻿using Qiniu.IO;
using Qiniu.IO.Model;
using Qiniu.Util;
using UnityEngine;

public class QiniuTool
{
    /// <summary>
    /// 使用表单上传方式上传小文件
    /// </summary>
    public static void UploadFile(string keyName,string filePath)
    {
        Qiniu.Common.Config.SetZone(Qiniu.Common.ZoneID.CN_North, true);
        // 生成(上传)凭证时需要使用此Mac
        // 这个示例单独提供了一个Settings类，其中包含AccessKey和SecretKey
        // 实际应用中，请自行设置您的AccessKey和SecretKey
        Mac mac = new Mac("4f6E32IsnmlTksRF3p0VakkGIAs6dzoS8tNikAvJ", "1x5l9xTVoK9oqEgauG1xqaCwutoYBEhDfkvkxbaX");

        string bucket = "ball-sort-puzzle";
        //string saveKey = "测试.png";
        //string localFile = "D:/res/ball/圆.png";

        // 上传策略，参见 
        // http://developer.qiniu.com/article/developer/security/put-policy.html
        PutPolicy putPolicy = new PutPolicy();

        // 如果需要设置为"覆盖"上传(如果云端已有同名文件则覆盖)，请使用 SCOPE = "BUCKET:KEY"
        // putPolicy.Scope = bucket + ":" + saveKey;
        putPolicy.Scope = bucket;

        // 上传策略有效期(对应于生成的凭证的有效期)          
        putPolicy.SetExpires(3600);

        // 上传到云端多少天后自动删除该文件，如果不设置（即保持默认默认）则不删除
        //putPolicy.DeleteAfterDays = 1;

        // 将PutPolicy转换为JSON字符串
        string jstr = putPolicy.ToJsonString();

        // 生成上传凭证，参见
        // http://developer.qiniu.com/article/developer/security/upload-token.html            
        string token = Auth.CreateUploadToken(mac, jstr);

        FormUploader fu = new FormUploader();


        // 支持自定义参数
        //var extra = new Dictionary<string, string>();
        //extra.Add("FileType", "UploadFromLocal");
        //extra.Add("YourKey", "YourValue");

        var result = fu.UploadFile(filePath, keyName, token);

        Debug.Log(result);
    }
}