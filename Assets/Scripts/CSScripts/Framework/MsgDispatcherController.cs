﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZLMsg;

namespace ZL_Framework
{
    public class MsgDispatcherController : Singletons<MsgDispatcherController>, IMsgReceiver, IMsgSender
    {
        /// <summary>
        /// 注册消息事件
        /// </summary>
        /// <param name="msgName">消息名</param>
        /// <param name="callback">事件</param>
        public void RegisterMsg(int msgName, Action<IMsgParam> callback = null)
        {
            this.RegisterLogicMsg(msgName, callback);
        }

        /// <summary>
        /// 反注册消息事件
        /// </summary>
        /// <param name="msgName">消息名</param>
        /// <param name="callback">事件</param>
        public void UnRegisterMsg(int msgName, Action<IMsgParam> callback)
        {
            this.UnRegisterLogicMsg(msgName, callback);
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="msgName">消息名</param>
        /// <param name="paramList">参数</param>
        public void SendMsg(int msgName, IMsgParam paramList)
        {
            this.SendLogicMsg(msgName, paramList);
        }
    }
}