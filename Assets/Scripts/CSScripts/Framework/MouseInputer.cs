﻿/*      使用方法：
 *      1.初始化MouseInputer 
 *          MouseInputer mMouseInputer = MouseInputer.Instance;
 *      2.在任意一个Update中调用mMouseInputer.CheckInteracting();
 *      3.
 *          mMouseInputer.Init(true, 3, layerMask);
 *          mMouseInputer.onClickObjDown = OnClickDown;
 *          mMouseInputer.onClickObjUp = OnClickUp;
 *          mMouseInputer.onEnter = OnEnter;
 *          mMouseInputer.OpenInput();
 *
 *      4.不需要的时候关闭：
 *          mMouseInputer.CloseInput();
 *      5.完全不需要的时候销毁：
 *          mMouseInputer.Dispose();
 */

using System;
using UnityEngine;
using UnityEngine.Events;

namespace ZL_Framework
{
    /// <summary>
    /// 非UI鼠标事件
    /// </summary>
    public class MouseInputer : Singletons<MouseInputer>
    {
        #region Action
        /// <summary> The on drag. </summary>
        public UnityAction<InputData> onDrag = null;
        /// <summary> The on drag end. </summary>
        public UnityAction<InputData> onDragEnd = null;
        /// <summary> The on drag start. </summary>
        public UnityAction<InputData> onDragStart = null;
        /// <summary> The on click object up. </summary>
        public UnityAction<InputData> onClickObjUp = null;
        /// <summary> The on click object down. </summary>
        public UnityAction<InputData> onClickObjDown = null;
        /// <summary> The on click object. </summary>
        public UnityAction<GameObject> onClickObjStay = null;
        public UnityAction<GameObject> onClickObj = null;

        /// <summary> The on enter object. </summary>
        public UnityAction<GameObject> onEnter = null;
        /// <summary> The on exit object. </summary>
        public UnityAction<GameObject> onExit = null;
        /// <summary> The on stay object. </summary>
        public UnityAction<GameObject> onStay = null;
        /// <summary> The on click none down. </summary>
        public UnityAction<InputData> onClickNoneDown = null;
        /// <summary> The on click none up. </summary>
        public UnityAction<InputData> onClickNoneUp = null;
        /// <summary> The on click none stay. </summary>
        public UnityAction<InputData> onClickNoneStay = null;
        #endregion

        private bool mIsEnter = false;
        private bool mIsDrag, mIsDragStart;
        private float mMouseDis;
        private Ray mInputRay;
        private RaycastHit mInputHit;
        private GameObject mHitTarget;
        private GameObject mOldTarget;
        private Camera mCamera;
        private Transform mTargetTF;
        private InputData mInputData = null;
        private Vector3 mMousePos;
        private Vector3 mMouseDownPos;
        private Vector3 mScreenSpace, mOffset;

        public MouseInputer()
        {
            mInputData = new InputData();
        }

        /// <summary>
        /// 打开 Input
        /// </summary>
        public void OpenInput()
        {
            if (mCamera == null)
            {
                mCamera = Camera.main;
            }
            IsOpenInput = true;
        }

        private bool mIsNullStart;
        public bool IsNullStart
        {
            get { return mIsNullStart; }
            set { mIsNullStart = value; }
        }

        /// <summary>
        /// 关闭 Input
        /// </summary>
        public void CloseInput()
        {
            IsOpenInput = false;
        }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="_isOpen">是否开启</param>
        /// <param name="_clickRange">点击范围(当鼠标按下之后鼠标移动的范围，当大于此范围时会判断为拖拽)</param>
        /// <param name="_layerMask">屏蔽层</param>
        public void Init(bool _isOpen, float _clickRange, LayerMask _layerMask)
        {
            Init(_isOpen, _clickRange);
            LayerMask = _layerMask;
        }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="_isOpen">是否开启</param>
        /// <param name="_clickRange">点击范围(当鼠标按下之后鼠标移动的范围，当大于此范围时会判断为拖拽)</param>
        public void Init(bool _isOpen, float _clickRange)
        {
            //Dispose();
            if (_isOpen)
            {
                OpenInput();
            }
            ClickRange = _clickRange;
        }

        public void CheckInteracting()
        {
            if (!IsOpenInput)
            {
                return;
            }

            if (mCamera == null)
            {
                mCamera = Camera.main;
            }
            if (mInputData == null)
            {
                mInputData = new InputData();
            }

            mMousePos = Input.mousePosition;
            mInputRay = mCamera.ScreenPointToRay(mMousePos);

            if (Physics.Raycast(mInputRay, out mInputHit, float.MaxValue, LayerMask))
            {
                mHitTarget = mInputHit.transform.gameObject;
                if (Input.GetMouseButtonDown(0))
                {
                    mIsDragStart = false;

                    mMouseDownPos = Input.mousePosition;
                    mTargetTF = mInputHit.transform;
                    mScreenSpace = mCamera.WorldToScreenPoint(mTargetTF.position);

                    Vector3 screenPos = new Vector3(mMousePos.x, mMousePos.y, mScreenSpace.z);
                    mOffset = mTargetTF.position - mCamera.ScreenToWorldPoint(screenPos);

                    mInputData.screenPosition = mMousePos;
                    mInputData.target = mTargetTF;
                    onClickObjDown?.Invoke(mInputData);
                }
                if (!mIsEnter)
                {
                    onEnter?.Invoke(mHitTarget);
                    mIsEnter = true;
                }
                onStay?.Invoke(mHitTarget);
                if ((mOldTarget != mHitTarget) && (mOldTarget != null))
                {
                    SetExit();
                }
                mOldTarget = mHitTarget;
            }
            else
            {
                SetExit();
                mHitTarget = null;

                mInputData.screenPosition = mMousePos;
                mInputData.target = null;
                if (Input.GetMouseButtonDown(0))
                {
                    mMouseDownPos = Input.mousePosition;
                    onClickNoneDown?.Invoke(mInputData);
                }
                if (Input.GetMouseButton(0))
                {
                    mMouseDis = Vector3.Distance(mMouseDownPos, mMousePos);
                    if (mIsDrag == false && mMouseDis < ClickRange)
                    {
                        onClickNoneStay?.Invoke(mInputData);
                    }
                    else
                    {
                        if (!mIsDragStart)
                        {
                            onDragStart?.Invoke(mInputData);
                            mIsDragStart = true;
                        }
                        mIsDrag = true;
                        onDrag?.Invoke(mInputData);
                    }
                }
                if (Input.GetMouseButtonUp(0))
                {
                    mMouseDis = Vector3.Distance(mMouseDownPos, mMousePos);
                    if (mMouseDis < ClickRange && !mIsDrag)
                    {
                        onClickNoneUp?.Invoke(mInputData);
                    }
                    else
                    {
                        onDragEnd?.Invoke(mInputData);
                    }
                    mTargetTF = null;
                    mIsDrag = false;
                }
            }

            if (mTargetTF == null && !mIsNullStart)
            {
                return;
            }

            if (Input.GetMouseButton(0))
            {
                if (Physics.Raycast(mInputRay, out mInputHit, float.MaxValue, LayerMask))
                {
                    onClickObj?.Invoke(mInputHit.transform.gameObject);
                }
                mMouseDis = Vector3.Distance(mMouseDownPos, mMousePos);
                if (mIsDrag == false && mMouseDis < ClickRange)
                {
                    onClickObjStay?.Invoke(mTargetTF.gameObject);
                }
                else
                {
                    if (!mIsDragStart)
                    {
                        onDragStart?.Invoke(mInputData);
                        mIsDragStart = true;
                    }
                    mIsDrag = true;
                    Vector3 curScreenSpace = new Vector3(mMousePos.x, mMousePos.y, mScreenSpace.z);
                    Vector3 CurPosition = mCamera.ScreenToWorldPoint(curScreenSpace) + mOffset;

                    mInputData.target = mTargetTF;
                    mInputData.position = CurPosition;
                    mInputData.screenPosition = curScreenSpace;
                    onDrag?.Invoke(mInputData);
                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                mMouseDis = Vector3.Distance(mMouseDownPos, mMousePos);
                if (mInputData != null)
                {
                    mInputData.target = mTargetTF;
                }
                if (mMouseDis < ClickRange && !mIsDrag)
                {
                    onClickObjUp?.Invoke(mInputData);
                }
                else
                {
                    onDragEnd?.Invoke(mInputData);
                }
                mTargetTF = null;
                mIsDrag = false;
            }
        }

        private void SetExit()
        {
            if (mIsEnter)
            {
                onExit?.Invoke(mOldTarget);
                mIsEnter = false;
                mOldTarget = null;
            }
        }

        public override void Dispose()
        {
            CloseInput();
            mCamera = null;
            mInputData = null;
            mHitTarget = null;
            mOldTarget = null;
            mMouseDis = 5;
            mIsNullStart = false;

            onDrag = null;
            onDragEnd = null;
            onDragStart = null;
            onClickObjUp = null;
            onClickObjDown = null;
            onClickObjStay = null;
            onEnter = null;
            onExit = null;
            onStay = null;
            onClickObj = null;
            onClickNoneDown = null;
            onClickNoneUp = null;
            onClickNoneStay = null;
        }

        /// <summary>
        /// 是否开启 Input
        /// </summary>
        /// <value><c>true</c> if is open input; otherwise, <c>false</c>.</value>
        public bool IsOpenInput { get; set; }
        public float ClickRange { get; set; }
        private LayerMask LayerMask { get; set; }
    }

    public class InputData
    {
        /// <summary>
        /// 作用物体
        /// </summary>
        public Transform target = null;
        /// <summary>
        /// Input worldPosition
        /// </summary>
        public Vector3 position = Vector3.zero;
        /// <summary>
        /// Input screenPosition
        /// </summary>
        public Vector2 screenPosition = Vector2.zero;
    }
}