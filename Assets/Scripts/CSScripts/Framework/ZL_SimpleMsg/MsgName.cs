﻿/****************************************************************************
 * Copyright (c) 2019.4 zhaoliang
 * 
 * https://github.com/lfzl000/SimpleMsgDispatcher
 *
 ****************************************************************************/

namespace ZLMsg
{
    //不要用0
    public class MsgName
    {
        public const int MSG_GAME_START = 1000;     //游戏开始
        public const int MSG_CLOSE_LOGO_PAGE = 1001;    //关闭LogoPage
        public const int MSG_RES_UPDATE_END = 1002;    //关闭LogoPage
        public const int MSG_GAME_HINT = 1005;   //提示
        public const int MSG_UPDATE_SCORE = 1010;   //更新分数
    }
}