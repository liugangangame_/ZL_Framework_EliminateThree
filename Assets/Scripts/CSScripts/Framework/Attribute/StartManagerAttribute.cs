﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZL_Framework
{
    /// <summary>
    /// 预加载管理器(添加此标签之后会在项目启动时就把脚本挂到场景里)
    /// </summary>
    public class StartManagerAttribute : Attribute
    {
    }
}