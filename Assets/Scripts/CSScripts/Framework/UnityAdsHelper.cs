﻿#if ADS

using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using ZL_Framework;
using ZLMsg;
#if UNITY_IOS || UNITY_ANDROID
using UnityEngine.Advertisements;
#endif

[StartManagerAttribute]
public class UnityAdsHelper : MonoSingleton<UnityAdsHelper>, IUnityAdsListener, IMsgSender
{
    public bool isHaveAds =
#if ADS
        true;
#else
        false;
#endif

    public string GameID
    {
        get
        {
#if UNITY_ANDROID
            return "3821267";
#elif UNITY_IOS
            return "3821266";
#endif
        }
    }
    public bool enableTestMode = false;

    protected override void Init()
    {
        if (!isHaveAds)
            return;

        Advertisement.AddListener(this);
        Advertisement.Initialize(GameID, enableTestMode);
    }

    public void ReadyCustomAds(string _placementId, UnityAction ready = null, bool isPlay = true)
    {
        if (!isHaveAds)
            return;
        StartCoroutine(WaitReadyAds(_placementId, ready, isPlay));
    }

    public void ReadyBannerAds(UnityAction ready = null, bool isPlay = true, BannerPosition bannerPosition = BannerPosition.BOTTOM_CENTER)
    {
        if (!isHaveAds)
            return;
        StartCoroutine(WaitReadyAds(AdsPlacementId.BannerAds, ready, isPlay, bannerPosition));
    }

    public void HideBannerAds(bool destroy)
    {
        Advertisement.Banner.Hide(destroy);
    }

    private IEnumerator WaitReadyAds(string placementId, UnityAction ready, bool isPlay, BannerPosition bannerPosition = BannerPosition.BOTTOM_CENTER)
    {
        while (!Advertisement.isInitialized)
        {
            yield return 0;
        }
        while (!Advertisement.IsReady(placementId))
        {
            yield return 0;
        }
        ready?.Invoke();

        if (isPlay)
        {
            if (placementId != AdsPlacementId.BannerAds)
            {
                Advertisement.Show(placementId);
            }
            else
            {
                Advertisement.Banner.SetPosition(bannerPosition);
                Advertisement.Banner.Show(placementId);
            }
        }
    }

    public void OnUnityAdsReady(string placementId)
    {
        LogTool.Instance.Log("Ads Ready " + placementId);
    }

    public void OnUnityAdsDidError(string message)
    {
    }

    public void OnUnityAdsDidStart(string placementId)
    {
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        AdsFinishResult adsFinishResult = new AdsFinishResult
        {
            placementId = placementId,
            ShowResult = showResult
        };
        this.SendLogicMsg(MsgName.MSG_ADS_FINISH, adsFinishResult);

        if (showResult == ShowResult.Failed)
        {
            LogTool.Instance.Log(placementId + " Failed");
        }
        else if (showResult == ShowResult.Finished)
        {
            LogTool.Instance.Log(placementId + " Finished");
        }
        else
        {
            LogTool.Instance.Log(placementId + " Skipped");
        }
    }

    protected override void Destroy()
    {
        //Advertisement.RemoveListener(this);
    }

    public class AdsFinishResult: IMsgParam
    {
        public string placementId;
        public ShowResult ShowResult;
    }
}

public class AdsPlacementId
{
    public const string BannerAds = "banner";
    public const string PassMissionAds = "pass_mission_ads";
    public const string BackOffAds = "back_off_ads";
    public const string RefreshAds = "refresh_ads";
}
#endif