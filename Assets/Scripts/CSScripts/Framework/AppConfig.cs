﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace ZL_Framework
{
    public class AppConfig
    {
        /*
         * 开发调试的话，让 IsSimulate = true，有改动的话直接运行就可以看到
         * 开发完成测试资源打包的话，可以把 IsSimulate = false，然后 IsEmulation = false，此时将从StreamingAssets目录下加载资源及Lua脚本
         * 如果想测试真实环境，把 IsSimulate = false，然后 IsEmulation = true，此时将会热更新资源包，并从缓存目录加载资源及Lua脚本
         * 
         * 如果在移动端，没有远程服务器的话，可以 IsEmulation 设置为false，这样在移动端也从StreamingAssets目录下加载资源及Lua脚本
        */

        /// <summary>
        /// 是否是虚拟加载（仅编辑器环境有效！为ture时直接从Asset/AssetBundles目录下获取资源，为false时会读取AB包资源）
        /// </summary>
        public static bool IsSimulate = true;
        /// <summary>
        /// 是否仿真加载（为ture时直接从本项目缓存目录下加载AB包，为false时从StreamingAssets目录加载AB包资源）
        /// </summary>
        public static bool IsEmulation = false;

        public static string baseUrl = "https://xxxxxxxxxxxx";
        /// <summary>
        /// 资源地址
        /// </summary>
        public static string ResPath = "Assets/AssetBundles";

        public static bool isLua = true;

        public static string ABPath
        {
            get
            {
                string path = "";
//#if UNITY_EDITOR
                if(IsEmulation)
                {
                    path = Path.Combine(Application.persistentDataPath, "serverassets");
                }
                else
                {
                    path = Path.Combine(Application.streamingAssetsPath, "serverassets");
                }
//#else
//                path = Path.Combine(Application.persistentDataPath, "serverassets");
//#endif
                return path;
            }
        }

        /// <summary>
        /// Lua脚本地址
        /// </summary>
        public static string LuaFilePath
        {
            get
            {
                string path = "";
//#if UNITY_EDITOR
                if (IsSimulate && Application.isEditor)
                {
                    path = Application.dataPath + "/Scripts/LuaScripts/";
                }
                else
                {
                    if (IsEmulation)
                    {
                        path = Application.persistentDataPath + "/serverassets/luascripts/";
                    }
                    else
                    {
                        path = Application.streamingAssetsPath + "/serverassets/luascripts/";
                    }
                }
//#else
//                path = Application.persistentDataPath + "/serverassets/luascripts/";

//#endif
                return path;
            }
        }

        /// <summary>
        /// 检测是否有网
        /// </summary>
        public static bool IsNet
        {
            get
            {
                return GetNetState() != NET_STATE_DISNET;
            }
        }

        /// <summary> 没网 </summary>
        public const int NET_STATE_DISNET = -1;
        /// <summary> 手机数据流量 </summary>
        public const int NET_STATE_MOBILE = 1;
        /// <summary> WIFI/网线 </summary>
        public const int NET_STATE_WIFI = 2;

        /// <summary>
        /// 获取网络状态
        /// </summary>
        /// <returns></returns>
        public static int GetNetState()
        {
            int netCode = NET_STATE_DISNET;
            switch (Application.internetReachability)
            {
                case NetworkReachability.NotReachable:
                    netCode = NET_STATE_DISNET;
                    break;
                case NetworkReachability.ReachableViaCarrierDataNetwork:
                    netCode = NET_STATE_MOBILE;
                    break;
                case NetworkReachability.ReachableViaLocalAreaNetwork:
                    netCode = NET_STATE_WIFI;
                    break;
                default:
                    netCode = NET_STATE_DISNET;
                    break;
            }
            return netCode;
        }
    }
}