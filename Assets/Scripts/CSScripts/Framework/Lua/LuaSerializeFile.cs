﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LitJson;
using UnityEngine;

[Serializable]
public class LuaFileObject
{
    [SerializeField]
    public string fileName;
    [SerializeField]
    public byte[] fileBytes;
}

public class LuaSerializeFile : ScriptableObject
{
    [SerializeField]
    public List<LuaFileObject> files = new List<LuaFileObject>();
    public Dictionary<string, byte[]> fileMap = new Dictionary<string, byte[]>();

    public void AddLuaFile(string fileName, byte[] fileBytes)
    {
        files.Add(new LuaFileObject() { fileName = fileName, fileBytes = fileBytes });
    }

    public void ConvertFileMap()
    {
        fileMap = new Dictionary<string, byte[]>();
        foreach (LuaFileObject obj in files)
        {
            if (obj == null)
            {
                continue;
            }

            if (fileMap.ContainsKey(obj.fileName))
            {
                fileMap.Remove(obj.fileName);
            }
            fileMap.Add(obj.fileName, obj.fileBytes);
        }
    }
}