﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZL_Framework
{
    public class UIType
    {
        /// <summary>
        /// Game层下面的UI
        /// </summary>
        public const int BackUI = 0;
        /// <summary>
        /// Game层上面的UI
        /// </summary>
        public const int TopUI = 1;
        /// <summary>
        /// 最顶层UI
        /// </summary>
        public const int PopUI = 2;
    }

    public class UIConst
    {

        public const string UI_LOGO_PAGE = "LogoPage";
    }
}