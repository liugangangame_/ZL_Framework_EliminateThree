﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZL_Framework
{
    /// <summary>
    /// LuaUI面板基类
    /// </summary>
    public class LuaUIPanel : LuaBehaviour, IUIPanel
    {
        public Action<IUIParam> luaOnOpen = null;
        public Action luaOnEnter = null;
        public Action luaOnExit = null;
        public Action luaDispose = null;

        public string UIName { get; set; }
        public int UIType { get; set; }
        public GameObject PanelObj { get; set; }

        private bool isActive;
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
                gameObject.SetActive(isActive);
                if(isActive)
                {
                    luaOnEnter?.Invoke();
                }
                else
                {
                    luaOnExit?.Invoke();
                }
            }
        }

        public override void Init(string scriptName)
        {
            base.Init(scriptName);

            luaOnOpen = scriptEnv.GetInPath<Action<IUIParam>>(scriptName + ".onOpen");
            luaOnEnter = scriptEnv.GetInPath<Action>(scriptName + ".onEnter");
            luaOnExit = scriptEnv.GetInPath<Action>(scriptName + ".onExit");
            luaDispose = scriptEnv.GetInPath<Action>(scriptName + ".disopse");

            UIName = scriptName;
            scriptEnv.SetInPath(scriptName + ".UIName", UIName);
            scriptEnv.SetInPath(scriptName + ".UIType", UIType);
        }

        public override void OnDestroy()
        {
            luaDispose?.Invoke();

            luaOnOpen = null;
            luaOnEnter = null;
            luaOnExit = null;
            luaDispose = null;

            base.OnDestroy();
        }

        public void OnOpen(IUIParam param)
        {
            luaOnOpen?.Invoke(param);
            OnEnter();
        }

        public void OnEnter()
        {
            luaOnEnter?.Invoke();
        }

        public void OnExit()
        {
            luaOnExit?.Invoke();
        }

        public void Dispose()
        {
            luaDispose?.Invoke();
        }

        public void Close()
        {
            UIManager.Instance.CloseUI(UIName);
        }
    }
}