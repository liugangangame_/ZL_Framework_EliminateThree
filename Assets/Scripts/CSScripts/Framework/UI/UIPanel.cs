﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZL_Framework
{
    public interface IUIPanel
    {
        string UIName { get; set; }
        int UIType { get; set; }
        bool IsActive { get; set; }
        /// <summary>
        /// UI创建完成时调用一次
        /// </summary>
        /// <param name="param"></param>
        void OnOpen(IUIParam param);
        /// <summary>
        /// UI每次设置为可见时调用
        /// </summary>
        void OnEnter();
        /// <summary>
        /// UI退出或不可见时调用
        /// </summary>
        void OnExit();
        /// <summary>
        /// UI销毁时调用
        /// </summary>
        void Dispose();
        GameObject PanelObj { get; set; }
    }

    public class UIPanel : MonoBehaviour, IUIPanel
    {

        public virtual string UIName { get; set; }
        public virtual int UIType { get; set; }
        public GameObject PanelObj { get; set; }
        private bool isActive;
        public bool IsActive
        {
            get
            { 
                return isActive;
            }
            set 
            { 
                isActive = value; 
                gameObject.SetActive(isActive); 
                if(isActive)
                {
                    OnEnter();
                }
                else
                {
                    OnExit();
                }
            }
        }

        public virtual void OnOpen(IUIParam param)
        {
            OnEnter();
        }

        public virtual void OnEnter()
        {

        }

        public virtual void OnExit()
        {

        }

        public virtual void Dispose()
        {

        }

        public virtual void Close()
        {
            UIManager.Instance.CloseUI(UIName);
        }

        public virtual void OnDestroy()
        {
            Dispose();
        }
    }
}