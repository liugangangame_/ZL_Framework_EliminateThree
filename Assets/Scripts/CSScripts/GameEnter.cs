﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace ZL_Framework.Game
{
    public class GameEnter : MonoBehaviour
    {
        private void Awake()
        {
            StartManager();
        }

        private void Start()
        {
            //游戏入口
            LogTool.Instance.Log("Start");

            LoadLogoPage();
        }

        /// <summary>
        /// 加载LogoPage
        /// </summary>
        private void LoadLogoPage()
        {
            UIManager.Instance.OpenUI(UIConst.UI_LOGO_PAGE);
        }

        /// <summary>
        /// 获取需要预加载的类
        /// </summary>
        private void StartManager()
        {
            Assembly assembly = Assembly.Load("Assembly-CSharp");
            Type[] types = assembly.GetTypes();
            foreach (Type t in types)
            {
                var att = t.GetCustomAttribute<StartManagerAttribute>();
                if (att != null)
                {
                    var singletonRootGo = GameObject.Find("MonoSingletonRoot");
                    if(singletonRootGo == null)
                    {
                        singletonRootGo = new GameObject("MonoSingletonRoot");
                    }
                    singletonRootGo.AddComponent(t);
                }
            }
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.C))
            {
                AudioManager.Instance.PlayCommonAudio("/Click.wav", () =>
                 {
                     Debug.Log("Click Played");
                 });
            }
        }
    }
}