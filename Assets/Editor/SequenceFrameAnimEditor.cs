﻿using System;
using UnityEditor;
using UnityEngine;

namespace ZL_Framework
{
    [CustomEditor(typeof(SequenceFrameAnim))]
    public class SequenceFrameAnimEditor : Editor
    {
        SequenceFrameAnim sequenceFrameAnim;

        public SerializedProperty animType, isLoop, playCount, isAwakePlay, isAutoDestroy, animSpeed, SpriteArray, TexArray, animComplete;

        private void OnEnable()
        {
            sequenceFrameAnim = (SequenceFrameAnim)target;

            animType = serializedObject.FindProperty("animType");
            isLoop = serializedObject.FindProperty("isLoop");
            playCount = serializedObject.FindProperty("playCount");
            isAwakePlay = serializedObject.FindProperty("isAwakePlay");
            isAutoDestroy = serializedObject.FindProperty("isAutoDestroy");
            animSpeed = serializedObject.FindProperty("animSpeed");
            SpriteArray = serializedObject.FindProperty("SpriteArray");
            TexArray = serializedObject.FindProperty("TexArray");
            animComplete = serializedObject.FindProperty("animComplete");
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.PropertyField(animType);
            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(isLoop);

            if (!isLoop.boolValue)
            {
                EditorGUILayout.PropertyField(playCount);
                if (playCount.intValue < 1)
                {
                    EditorGUILayout.HelpBox("播放次数不能小于1", MessageType.Error);
                }
                EditorGUILayout.PropertyField(isAutoDestroy);
            }

            EditorGUILayout.PropertyField(isAwakePlay);
            EditorGUILayout.PropertyField(animSpeed);
            if (animSpeed.floatValue < 0)
            {
                EditorGUILayout.HelpBox("播放速度不能小于0", MessageType.Error);
            }
            else if (animSpeed.floatValue < 1)
            {
                EditorGUILayout.HelpBox("播放速度为0时动画不会播放", MessageType.Warning);
            }

            if (animType.enumValueIndex == 0 || animType.enumValueIndex == 2)
            {
                EditorGUILayout.PropertyField(SpriteArray);
            }
            else if (animType.enumValueIndex == 1)
            {
                EditorGUILayout.PropertyField(TexArray);
            }

            if (!isLoop.boolValue)
            {
                EditorGUILayout.Space();
                EditorGUILayout.Space();
                EditorGUILayout.PropertyField(animComplete);
            }

            /*
            if(GUILayout.Button("预览"))
            {
            }
            if (GUILayout.Button("停止预览"))
            {
            }
            */

            serializedObject.ApplyModifiedProperties();
            serializedObject.Update();
        }
    }
}