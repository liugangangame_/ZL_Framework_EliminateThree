﻿using UnityEngine;
using UnityEditor;

public class EditorExtensions
{
    [MenuItem("Assets/打开缓存目录")]
    private static void OpenPersistentPath()
    {
        Application.OpenURL(Application.persistentDataPath);
    }

    [MenuItem("Assets/清除项目缓存(缓存目录下所有文件及 PlayerPrefs)")]
    private static void DeleteCache()
    {
        bool isTure = EditorUtility.DisplayDialog("清除缓存", "是否清除全部缓存（包含Log文件及PlayerPrefs）", "确定清除", "取消");
        if (isTure)
        {
            BuildAssetBundlesTool.DeleteFile(Application.persistentDataPath);
            PlayerPrefs.DeleteAll();
            Debug.Log("清除项目缓存完成！");
        }
    }

    [MenuItem("GameObject/序列帧动画", priority = 48)]
    private static void CreateZLSequenceAnim()
    {
        new GameObject("New Sequence Frame Anim", typeof(ZL_Framework.SequenceFrameAnim));
    }

    [MenuItem("GameObject/快速生成ZL_Framework场景所需物体", priority = 48)]
    private static void CreateZLSceneObj()
    {
        Object.DestroyImmediate(FindObj("Directional Light"));
        FindObj("GameEnter").AddComponent<ZL_Framework.Game.GameEnter>();
        Transform uiRoot = FindObj("UIRoot").transform;

        CreateUI(uiRoot, "BackUI");
        CreateUI(uiRoot, "PopUI");
        CreateUI(uiRoot, "TopUI");
        CreateUI(uiRoot, "Log");

        Transform logUI = uiRoot.Find("Log");
        ZL_Framework.LogTool logTool = Resources.Load<ZL_Framework.LogTool>("LogTool");
        if (logTool != null)
        {
            Object.Instantiate(logTool, logUI);
        }

        FindObj("EventSystem").AddComponent<UnityEngine.EventSystems.StandaloneInputModule>();

        Debug.Log("ZL_Framework场景创建完成！");
    }

    [MenuItem("Tools/Delete All PlayerPrefs")]
    private static void DeleteAllPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
        Debug.Log("Delete All PlayerPrefs");
    }

    private static GameObject FindObj(string objName)
    {
        GameObject obj;
        obj = GameObject.Find(objName);
        if (obj == null)
        {
            obj = new GameObject(objName);
        }
        return obj;
    }

    private static void CreateUI(Transform root, string ui)
    {
        Transform curUI = root.Find(ui);
        if (curUI == null)
        {
            GameObject newObj = new GameObject(ui);
            newObj.transform.parent = root;
            curUI = newObj.transform;
        }
        UnityEngine.UI.CanvasScaler canvasScaler = curUI.gameObject.GetComponent<UnityEngine.UI.CanvasScaler>();
        if (canvasScaler == null)
        {
            canvasScaler = curUI.gameObject.AddComponent<UnityEngine.UI.CanvasScaler>();
        }
        Canvas backCan = canvasScaler.gameObject.GetComponent<Canvas>();
        backCan.renderMode = RenderMode.ScreenSpaceCamera;
        backCan.worldCamera = Camera.main;
        backCan.sortingLayerName = ui;
        canvasScaler.uiScaleMode = UnityEngine.UI.CanvasScaler.ScaleMode.ScaleWithScreenSize;
        canvasScaler.referenceResolution = new Vector2(1080, 1920);
        UnityEngine.UI.GraphicRaycaster graphicRaycaster = canvasScaler.gameObject.GetComponent<UnityEngine.UI.GraphicRaycaster>();
        if (graphicRaycaster == null)
        {
            canvasScaler.gameObject.AddComponent<UnityEngine.UI.GraphicRaycaster>();
        }
    }
}