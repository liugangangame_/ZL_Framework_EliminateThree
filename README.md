# ZL_Framework - 一个unity的简易框架

这个框架是我本人利用闲暇之余从零开始搭建的，非常适合开发2D游戏和一些3D轻量游戏。可以满足一些基本的项目需求，适合初学者或者独立游戏开发。
> 注：本项目基于Unity2019.3.8f1版本开发，暂未验证其他版本通用性

> 注：由于使用了c#高级语法，请到 PlayerSettings - OtherSettings ，设置 API Level 为 .Net4.x

## 框架已实现的功能模块

* 资源打包
* 网络请求
* 非UI鼠标事件
* 消息机制
* 序列帧动画
* 对象池
* 音频管理
* xLua
  
## 未实现或正在实现的功能模块

* 资源加载(90% 热更)
* UI框架(80% 比较简陋，但是可以用了)
* 协程管理
* 配置管理

## START

文档说明请点击[这里](https://gitee.com/zlr710/ZL_Framework_EliminateThree/tree/master/Assets/Doc)
> 现在框架内部包含了一个3消游戏，可以作为参考